<?php

use Illuminate\Http\Request;
use App\Comment;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/test', function(){

    return "Selamat datang di Apliaski Web Service alias API pertama saya";
});


//Belajar API Web Service disini
Route::get('/post','postController@Index');
Route::post('/post','postController@Store');
Route::get('/post/{id}','postController@show');
Route::put('/post/{id}','postController@update');
Route::delete('/post/{id}','postController@destroy');


//Tugas API JWT -1
Route::get('/comment','commentController@Index');
Route::post('/comment','commentController@Store');
Route::get('/comment/{id}','commentController@Show');
Route::put('/comment/{id}','commentController@update');
Route::delete('/comment/{id}','commentController@destroy');
