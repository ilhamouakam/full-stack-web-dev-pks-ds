<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('name');

            // $table->uuid('role_id')->nullable();
            // $table->foreign('role_id')->refereces('id')->on('roles');
            

            // $table->timestamp('email_verified_at')->nullable();
            // $table->string('password');


            $table->rememberToken();
            $table->timestamps();


            $table->uuid('roles_id');
            $table->foreign('roles_id')->references('id')->on('roles');
        });
    }

    /** 
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
