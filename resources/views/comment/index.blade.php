<a href="/comment/create" class="btn btn-success mb-2" >Tambah Data Comment</a>

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Content</th>
                <th scope="col">Aksi</th>
            </tr>
            </thead>
            <tbody>
                @forelse ($comments as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->content}}</td>

                    <td><a href="/comment/{{$item->id}}" class="btn btn-info btn-sm" >Detail</a>
                        <a href="/comment/{{$item->id}}/edit" class="btn btn-warning btn-sm" >Edit</a>
                        <form action="/comment/{{$item->id}}" method="POST">
                            @method('delete')
                            @csrf

                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">

                        </form>
                        
                    <td>
                </tr>
                    
                @empty
                <tr>
                    <td> Tidak ada DATA</td>
                </tr>
                    
                @endforelse
            </tbody>
        </table>