<?php

namespace App;


use App\Role;
use Illuminate\Support\Str;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass  assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username','roles_id','password','email_verified_at'
            ];
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {

        parent::boot();

        static::creating( function($model){

            if( empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()}=Str::uuid();
            }

            $model->roles_id = Role::where('name','author')->first()->id;  //ini kalau mau langsung isi Author di field Role nya 
        });

    }

    public function roles()
    {
        return $this->belongsTo('App\Role');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
