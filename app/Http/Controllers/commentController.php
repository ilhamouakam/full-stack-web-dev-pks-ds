<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;
use App\Comment;


class commentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()  //Menmpilkan semua isi 
    {
      

        $comments = DB::table('comment')->get();
         //return view('comment.index', compact('comments')); 
        return response()->json([

            'success'=> true,
            'message'=> 'Data Comment berhasil ditampilkan',
            'data'=>$comments
        ]);


       


        //dd($comments);  ini testing DD
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comments = Comment::create([
            'content'=>$request->content,
            'posts_id'=>$request->posts_id,
        ]);

        return response()->json([
            'success'=>true,
            'message'=>'Data Post berhasil di Input',
            'data'=>$posts

        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $comments = Comment::findOrFail($request->Id);

        return response()->json([
            'success'=>true,
            'message'=>'Data Post berhasil di Input',
            'data'=>$comments

        ],200);

        

        

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comments = comment::findOrFail($post->id);
        $comments->update([
            'content'     => $request->content,
            'posts_id'   => $request->posts_id
         ]);
         
         return response()->json([
            'success' => true,
            'message' => 'Post Updated',
            'data'    => $comments  
         ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comments = comment::findOrfail($id);

        $comments->delete();

        return response()->json([
        'success' => true,
        'message' => 'Post Deleted',
        ], 200);
    }
}
