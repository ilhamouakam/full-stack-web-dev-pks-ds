<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;


class postController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = DB::table('posts')->get();
        //return view('comment.index', compact('comments')); 
       return response()->json([

           'success'=> true,
           'message'=> 'Data POST berhasil ditampilkan',
           'data'=>$posts
       ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $posts = Post::create([
            'title'=>$request->title,
            'description'=>$request->description,
        ]);

        if ($posts){
            return response()->json([
                'success'=>true,
                'message'=>'Data Post berhasil di Input',
                'data'=>$posts
    
            ],200);

        }
        return response()->json([
            'success'=>false,
            'message'=>'Data Post TIDAK berhasil di Input'          

        ],409);



        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $post=Post::findOrFail($request->id);

        return response()->json([
            'success'=>true,
            'message'=>'Data Post berhasil di Input',
            'data'=>$post

        ],200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
